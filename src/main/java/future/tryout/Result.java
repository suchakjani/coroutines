package future.tryout;

import lombok.Value;

@Value(staticConstructor = "of")
class Result {
    int time;
    boolean success;
    String message;
}

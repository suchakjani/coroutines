package future.tryout;

import java.util.Optional;
import java.util.concurrent.CompletionStage;

public class FutureExamples {

    private WorkManager manager = new WorkManager();

        public CompletionStage<Void> workWithRun(int time1, int time2) {
            return
                manager
                .workForTime(time1)
                .thenRun(() -> manager.workForTime(time2));
        }

    public CompletionStage<CompletionStage<Result>> workWithApply(int time1, int time2) {
        return
            manager
            .workForTime(time1)
            .thenApply(
                result -> manager.workForTime(time2) // If we wanted we could do something with result here
            );
    }

    public CompletionStage<Result> workWithCompose(int time1, int time2) {
        return
            manager
            .workForTime(time1)
            .thenCompose(
                result ->  manager.workForTime(time2) // If we wanted we could do something with result here
            );
    }

    public CompletionStage<Void> workWithAccept(int time1, int time2, int time3) {
        return
            manager
            .workForTime(time1)
            .thenAcceptBoth(
                manager.workForTime(time2), (result, result2) -> {
                    if (result.isSuccess() && result2.isSuccess()) {
                        manager.workForTime(time3);
                    }
                });
    }

    public CompletionStage<Optional<CompletionStage<Result>>> workWithCombine(int time1, int time2, int time3) {
        return
            manager
            .workForTime(time1)
            .thenCombine(manager.workForTime(time2), (result, result2) -> {
                if (result.isSuccess() && result2.isSuccess()) {
                    return Optional.of(manager.workForTime(time3));
                }
                return Optional.empty();
            });
    }

}

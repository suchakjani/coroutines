package future.tryout;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class WorkManager {

    private ExecutorService es = Executors.newCachedThreadPool();

    CompletionStage<Result> workForTime(int millis) {
        CompletableFuture<Result> future = new CompletableFuture<>();
        es.submit(()->{
            try {
                Thread.sleep(millis);
                future.complete(Result.of(millis, true, "Worked for " + millis + " milliseconds"));
            } catch (InterruptedException e) {
                future.complete(Result.of(millis, false, "Error : " + e.getMessage()));
            }
        });
        return future;
    }
}

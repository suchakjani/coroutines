package coroutine.tryout

import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

class CoroutineExamples {

    private val manager = WorkManager()

    suspend fun workWithTwoTimes(time1: Int, time2: Int): Pair<Result, Result> {
        return coroutineScope {
            val result1 = manager.workForTime(time1) // work1
            val result2 = manager.workForTime(time2) // work2 after work1
            Pair(result1, result2)
        }
    }

    suspend fun workWithTwoTimesParallel(time1: Int, time2: Int): Pair<Result, Result> {
        return coroutineScope {
            val work1 = async { manager.workForTime(time1) } // Async work1
            val result2 = manager.workForTime(time2) // work2 while work1 is working
            val result1 = work1.await() // non-blocking wait
            Pair(result1, result2)
        }
    }

    suspend fun workWithThreeTimes(time1: Int, time2: Int, time3: Int): Triple<Result, Result, Result> {
        return coroutineScope {
            val result1 = manager.workForTime(time1) // work1
            val result2 = manager.workForTime(time2) // work2 after work1
            val result3 = manager.workForTime(time3) // work3 after work2
            Triple(result1, result2, result3)
        }
    }

    suspend fun workWithThreeTimesParallel(time1: Int, time2: Int, time3: Int): Triple<Result, Result, Result> {
        return coroutineScope {
            val work1 = async { manager.workForTime(time1) } // Async work1
            val work2 = async { manager.workForTime(time2) } // Async work2 while work1 is working
            val result3 = manager.workForTime(time3) // work3 while work1 and work2 are working
            val result1 = work1.await() // non-blocking wait
            val result2 = work2.await()// non-blocking wait
            Triple(result1, result2, result3)
        }
    }
    // async await is a bad work in java as it blocks threads, however in kotlin "Awaits for completion of this value without blocking a thread"
    // how could you do this in java, well akka actors, when work finishes, end a notification, vertx eevents etc...
}

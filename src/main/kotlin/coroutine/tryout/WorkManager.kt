package coroutine.tryout

import kotlinx.coroutines.delay

internal class WorkManager {

    suspend fun workForTime(millis: Int): Result {
        delay(millis.toLong())
        return Result(millis, true, "Worked for $millis milliseconds")
    }
}

// read
//https://kotlinlang.org/docs/reference/coroutines/cancellation-and-timeouts.html
//https://kotlinlang.org/docs/reference/coroutines/exception-handling.html


package coroutine.tryout

data class Result(val time: Int, val isSuccess: Boolean, val message: String)

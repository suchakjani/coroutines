package future.tryout;


import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.*;

class FutureExamplesTest {
    private FutureExamples futureExamples = new FutureExamples();
    private int time1 = 300;
    private int time2 = 200;
    private int time3 = 100;

    @Test
    void workWithRunTest() throws InterruptedException, ExecutionException {
        CompletionStage<Void> answer = futureExamples.workWithRun(time1, time2);
        answer.toCompletableFuture().get();
        errorChecks(answer);
    }

    @Test
    void workWithApplyTest() throws ExecutionException, InterruptedException {
        CompletionStage<CompletionStage<Result>> answer = futureExamples.workWithApply(time1, time2);
        CompletionStage<Result> stage = answer.toCompletableFuture().get();
        assertNotNull(stage);
        errorChecks(answer);
        Result result = stage.toCompletableFuture().get();
        assertNotNull(result);
        errorChecks(stage);
        assertTrue(result.isSuccess());
        assertEquals(time2, result.getTime());
    }

    @Test
    void workWithComposeTest() throws InterruptedException, ExecutionException {
        CompletionStage<Result> answer = futureExamples.workWithCompose(time1, time2);
        Result result = answer.toCompletableFuture().get();
        assertNotNull(result);
        errorChecks(answer);
        assertTrue(result.isSuccess());
        assertEquals(time2, result.getTime());
    }

    @Test
    void workWithAcceptTest() throws ExecutionException, InterruptedException {
        CompletionStage<Void> answer = futureExamples.workWithAccept(time1, time2, time3);
        answer.toCompletableFuture().get();
        errorChecks(answer);
    }

    @Test
    void workWithCombineTest() throws ExecutionException, InterruptedException {
        CompletionStage<Optional<CompletionStage<Result>>> answer = futureExamples.workWithCombine(time1, time2, time3);
        Optional<CompletionStage<Result>> stage1 = answer.toCompletableFuture().get();
        assertNotNull(stage1);
        errorChecks(answer);
        assertTrue(stage1.isPresent());
        CompletionStage<Result> stage2 = stage1.get();
        assertNotNull(stage2);
        Result result = stage2.toCompletableFuture().get();
        assertNotNull(result);
        errorChecks(stage2);
        assertTrue(result.isSuccess());
        assertEquals(time3, result.getTime());
    }

    private void errorChecks(CompletionStage answer) {
        assertTrue(answer.toCompletableFuture().isDone());
        assertFalse(answer.toCompletableFuture().isCancelled());
        assertFalse(answer.toCompletableFuture().isCompletedExceptionally());
    }
}

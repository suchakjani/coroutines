package coroutine.tryout

import io.kotlintest.specs.StringSpec
import kotlin.system.measureTimeMillis


class CoroutineExamplesTimeTest : StringSpec({

    val coroutineExamples = CoroutineExamples()
    val time1 = 300
    val time2 = 200
    val time3 = 100

    "workWithTwoTimes should return result time1 and result time2" {
        val time = measureTimeMillis {
            coroutineExamples.workWithTwoTimes(time1, time2)
        }
        println("workWithTwoTimes in $time ms")
    }

    "workWithThreeTimes should return result time1, time2  and  time3" {
        val time = measureTimeMillis {
            coroutineExamples.workWithThreeTimes(time1, time2, time3)
        }
        println("workWithThreeTimes in $time ms")
    }

    "workWithTwoTimesParallel should return result time1 and result time2" {
        val time = measureTimeMillis {
            coroutineExamples.workWithTwoTimesParallel(time1, time2)
        }
        println("workWithTwoTimesParallel in $time ms")
    }

    "workWithThreeTimesParallel should return result time1, time2  and  time3" {
        val time = measureTimeMillis {
            coroutineExamples.workWithThreeTimesParallel(time1, time2, time3)
        }
        println("workWithThreeTimesParallel in $time ms")
    }

})

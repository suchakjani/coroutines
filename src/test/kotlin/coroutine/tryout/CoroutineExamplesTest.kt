package coroutine.tryout

import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec


class CoroutineExamplesTest : StringSpec({

    val coroutineExamples = CoroutineExamples()
    val time1 = 300
    val time2 = 200
    val time3 = 100

    "workWithTwoTimes should return result time1 and result time2" {
        val result = coroutineExamples.workWithTwoTimes(time1, time2)
        result.first.isSuccess shouldBe true
        result.second.isSuccess shouldBe true
        result.first.time shouldBe time1
        result.second.time shouldBe time2
    }

    "workWithThreeTimes should return result time1, time2  and  time3" {
        val result = coroutineExamples.workWithThreeTimes(time1, time2, time3)
        result.first.isSuccess shouldBe true
        result.second.isSuccess shouldBe true
        result.third.isSuccess shouldBe true
        result.first.time shouldBe time1
        result.second.time shouldBe time2
        result.third.time shouldBe time3
    }

    "workWithTwoTimesParallel should return result time1 and result time2" {
        val result = coroutineExamples.workWithTwoTimesParallel(time1, time2)
        result.first.isSuccess shouldBe true
        result.second.isSuccess shouldBe true
        result.first.time shouldBe time1
        result.second.time shouldBe time2
    }

    "workWithThreeTimesParallel should return result time1, time2  and  time3" {
        val result = coroutineExamples.workWithThreeTimesParallel(time1, time2, time3)
        result.first.isSuccess shouldBe true
        result.second.isSuccess shouldBe true
        result.third.isSuccess shouldBe true
        result.first.time shouldBe time1
        result.second.time shouldBe time2
        result.third.time shouldBe time3
    }
})
